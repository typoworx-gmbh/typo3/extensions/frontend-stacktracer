# TYPO3 Extension: Frontend Stacktracer

**What does it do**
Frontend middleware & backend-module to trace/view frontend stacktrace-errors

**Installation:**<br />
```composer req typoworx/frontend-stacktracer```
